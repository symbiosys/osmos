﻿using System.ComponentModel.DataAnnotations;

namespace Osmos.Models.Controllers
{
    public class LoginData
    {
        [Required, EmailAddress]
        public string Username { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class RegisterData {
        [Required, EmailAddress]
        public string Username { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        [Required, DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
