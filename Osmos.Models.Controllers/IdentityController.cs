﻿using Osmos.Models.Identity;
using System.Data.Entity;
using System.Web.Http;

namespace Osmos.Models.Controllers
{
    [Authorize]
    public abstract class IdentityController : ApiController
    {
        private IdentityDbContext _identityDb = null;
        private IdentityUserManager _userManager = null;
        private DbContext _db = null;

        public IdentityDbContext IdentityDb { get { return _identityDb; } }
        public IdentityUserManager UserManager { get { return _userManager; } }

        public DbContext Db { get { return _db; } }

        public IdentityController()
        {
            _identityDb = new IdentityDbContext();
            _userManager = IdentityUserManager.Create(new IdentityUserStore(_identityDb));

            //_db = new DbContext();
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null) {
                _db.Dispose();
                _db = null;
            }

            if (_userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            if (_identityDb != null)
            {
                _identityDb.Dispose();
                _identityDb = null;
            }

            base.Dispose(disposing);
        }
    }
}
