﻿using Osmos.IdentityServer.Helpers;
using Osmos.IdentityServer.Models;
using Osmos.Models.Identity.Helpers;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using Thinktecture.IdentityModel.Client;

namespace Osmos.Models.Controllers
{
    public abstract class AccountController : IdentityController
    {
        private string _tokenEndoint = null;
        private string _clientId = null;
        private string _clientSecret = null;

        public AccountController()
        {
            _tokenEndoint = Discovery.Get().TokenEndpoint;
            _clientId = ConfigurationManager.AppSettings["client_id"];
            _clientSecret = ConfigurationManager.AppSettings["client_secret"];
        }

        [AllowAnonymous]
        public virtual async Task<IHttpActionResult> LoginAsync(LoginData data)
        {
            if (data == null) return BadRequest(Messages.DataCanNotBeNull());

            if (!ModelState.IsValid) return BadRequest(Messages.BadArguments());

            var client = new OAuth2Client(
                new Uri(_tokenEndoint),
                _clientId,
                _clientSecret);

            var result = await client
                .RequestResourceOwnerPasswordAsync(data.Username, data.Password, "openid profile roles resource_api");

            if (result.IsHttpError)
                return InternalServerError(new Exception(result.HttpErrorReason));

            if (result.IsError)
                return BadRequest(result.Error);

            var userFromDb = await IdentityDb.Users.FirstOrDefaultAsync(u => u.UserName == data.Username);
            if (userFromDb == null) return InternalServerError();

            return Ok(new
            {
                AccessToken = result.AccessToken,
                ExpiresIn = result.ExpiresIn
            });
        }

        [AllowAnonymous]
        public virtual async Task<IHttpActionResult> RegisterAsync(RegisterData data)
        {

            if (data == null) return BadRequest(Messages.DataCanNotBeNull());

            if (!ModelState.IsValid) return BadRequest(Messages.BadArguments());

            var options = new CreateUserOptions {
                IdentityUserManager = UserManager,
                Username = data.Username,
                Password = data.Password
            };

            var user = await UserHelper.CreateUserAsync(options);

            return Ok();
        }

        public virtual async Task<IHttpActionResult> GetUserInfoAsync() {

            string userId = IdentityServerTools.GetUserId(User);

            var userFromDb = await IdentityTools.GetUserFromDbAsync(IdentityDb, userId);

            return Ok(userFromDb);
        }
    }
}
