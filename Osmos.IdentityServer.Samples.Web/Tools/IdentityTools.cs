﻿using Microsoft.AspNet.Identity;
using Osmos.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osmos.IdentityServer.Samples.Web.Tools
{
    public static class IdentityTools
    {
        public static IdentityUserManager CreateIdentityUserManager(IdentityUserStore store) {
            IdentityUserManager mgr = new IdentityUserManager(store);

            mgr.UserLockoutEnabledByDefault = true;
            mgr.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(15);
            mgr.MaxFailedAccessAttemptsBeforeLockout = 3;
            mgr.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonLetterOrDigit = true,
                RequireUppercase = true
            };

            return mgr;
        }
    }
}