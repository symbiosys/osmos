﻿using Osmos.Models.Controllers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Osmos.IdentityServer.Samples.Web.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : Osmos.Models.Controllers.AccountController
    {
        [Route("register")]
        public override async Task<IHttpActionResult> RegisterAsync(RegisterData data)
        {
            return await base.RegisterAsync(data);
        }

        [Route("login")]
        public override async Task<IHttpActionResult> LoginAsync(LoginData data)
        {
            return await base.LoginAsync(data);
        }

        [Route("userinfo")]
        public override async Task<IHttpActionResult> GetUserInfoAsync()
        {
            return await base.GetUserInfoAsync();
        }
    }
}