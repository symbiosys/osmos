﻿using System.Collections.Generic;
using System.Configuration;
using Thinktecture.IdentityServer.Core.Models;

namespace Osmos.IdentityServer.Models
{
    static class Clients
    {
        public static IEnumerable<Client> Get()
        {

            string clientId = ConfigurationManager.AppSettings["client_id"];
            string clientSecret = ConfigurationManager.AppSettings["client_secret"];

            string accessTokenLifetimeString = ConfigurationManager.AppSettings["access_token_lifetime"];
            int accessTokenLifetime = 0;
            int.TryParse(accessTokenLifetimeString, out accessTokenLifetime);

            return new[]
            {
                new Client
                {
                    Enabled = true,
                    ClientName = "Self",
                    ClientId = clientId,
                    ClientSecrets = new List<ClientSecret>{
                        new ClientSecret(clientSecret.Sha256())
                    },
                    Flow = Flows.ResourceOwner,
                    AccessTokenLifetime = accessTokenLifetime
                }
            };
        }
    }
}
