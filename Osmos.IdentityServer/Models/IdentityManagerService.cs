﻿using Osmos.Models.Identity;
using IdentityManager;
using IdentityManager.AspNetIdentity;
using IdentityManager.Configuration;

namespace Osmos.IdentityServer.Models
{
    static class IdentityManagerService
    {
        public static void ConfigureSimpleIdentityManagerService(this IdentityManagerServiceFactory factory)
        {
            factory.Register(new Registration<IdentityDbContext>(resolver => new IdentityDbContext()));
            factory.Register(new Registration<IdentityUserStore>());
            factory.Register(new Registration<IdentityRoleManager>());
            factory.Register(new Registration<IdentityUserManager>());
            factory.Register(new Registration<IdentityRoleManager>());
            factory.IdentityManagerService = new Registration<IIdentityManagerService, SimpleIdentityManagerService>();
        }
    }

    class SimpleIdentityManagerService : AspNetIdentityManagerService<
        Microsoft.AspNet.Identity.EntityFramework.IdentityUser,
        string, Microsoft.AspNet.Identity.EntityFramework.IdentityRole,
        string>
    {
        public SimpleIdentityManagerService(IdentityUserManager userMgr, IdentityRoleManager roleMgr)
            : base(userMgr, roleMgr)
        {
        }
    }
}
