﻿using System;
using System.Configuration;

namespace Osmos.IdentityServer.Models
{
    public class Discovery
    {
        private string _rootUrl = null;
        private string _discoveryEndpoint = null;
        private string _authority = null;
        private string _tokenEndpoint = null;

        public string RootUrl { get { return _rootUrl; } }
        public string Authority { get { return _authority; } }
        public string DiscoveryEndpoint { get { return _discoveryEndpoint; } }
        public string TokenEndpoint { get { return _tokenEndpoint; } }

        private static Discovery _latest = null;
        private static void _Init()
        {
            string rootUrl = ConfigurationManager.AppSettings["root_url"];
            if (rootUrl == null) throw new ArgumentNullException("Can not find the \"root_url\" key in the configuration file.");
            string origin = ConfigurationManager.AppSettings["origin"] ?? "identity";

            _latest = new Discovery
            {
                _rootUrl = rootUrl,
                _authority = rootUrl + "/" + origin,
                _discoveryEndpoint = rootUrl + "/" + origin + "/.well-known/openid-configuration",
                _tokenEndpoint = rootUrl + "/" + origin + "/connect/token"
            };
        }
        public static Discovery Get()
        {

            if (_latest == null) _Init();

            return _latest;
        }
    }
}
