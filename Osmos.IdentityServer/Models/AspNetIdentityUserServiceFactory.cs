﻿using Microsoft.AspNet.Identity.EntityFramework;
using Osmos.Models.Identity;
using Thinktecture.IdentityServer.AspNetIdentity;
using Thinktecture.IdentityServer.Core.Configuration;
using Thinktecture.IdentityServer.Core.Services;

namespace Osmos.IdentityServer.Models
{
    static class UserServiceExtensions
    {
        public static void ConfigureUserService(this IdentityServerServiceFactory factory)
        {
            factory.UserService = new Registration<IUserService, AspNetIdentityUserServiceFactory>();
            factory.Register(new Registration<IdentityUserManager>());
            factory.Register(new Registration<IdentityUserStore>());
            factory.Register(new Registration<Osmos.Models.Identity.IdentityDbContext>());
        }
    }

    class AspNetIdentityUserServiceFactory : AspNetIdentityUserService<IdentityUser, string>
    {
        public AspNetIdentityUserServiceFactory(IdentityUserManager userMgr)
            : base(userMgr)
        {
        }
    }
}
