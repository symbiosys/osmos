﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Osmos.IdentityServer.Helpers
{
    public static class IdentityServerTools
    {
        public static string GetUserId(IPrincipal user) {
            var tUser = user as ClaimsPrincipal;

            var claims = tUser.Claims
                            .Select(c => new
                            {
                                type = c.Type,
                                value = c.Value
                            });

            var userIdClaim = claims.FirstOrDefault(c => c.type == "sub");
            if (userIdClaim == null) throw new Exception();

            return userIdClaim.value;
        }
    }
}
