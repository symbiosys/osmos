﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Osmos.Models.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Osmos.IdentityServer.Helpers
{
    public class UserAlreadyExistsException : System.Exception
    {
        public UserAlreadyExistsException(string username)
            : base(string.Format("The user \"{0}\" already exists.", username)) { }
    }

    public class FailedToCreateTheUserException : System.Exception
    {
        public FailedToCreateTheUserException(IEnumerable<string> Errors)
        :base(string.Join("\n", Errors)){}
    }

    public static class UserHelper
    {
        public static async Task<IdentityUser> CreateUserAsync(CreateUserOptions options)
        {
            if (options == null) throw new ArgumentNullException("Options can not be null");

            var existingUser = await options.IdentityUserManager.FindByNameAsync(options.Username);
            if (existingUser != null) throw new UserAlreadyExistsException(options.Username);

            var newUser = new IdentityUser { 
                UserName = options.Username,
                Email = options.Username,
                EmailConfirmed = options.EmailConfirmed != null ? (bool)options.EmailConfirmed : false,
                PhoneNumber = options.PhoneNumber,
                PhoneNumberConfirmed = options.PhoneNumberConfirmed != null ? (bool)options.PhoneNumberConfirmed : false
            };

            IdentityResult result = await options.IdentityUserManager.CreateAsync(newUser, options.Password);

            if (options.Rolename != null && result.Succeeded)
            {
                await options.IdentityUserManager
                    .AddClaimAsync(newUser.Id,
                                    new Claim(Thinktecture.IdentityServer.Core.Constants.ClaimTypes.Role, options.Rolename));
            }

            if (!result.Succeeded) throw new FailedToCreateTheUserException(result.Errors);

            return newUser;
        }
    }
}
