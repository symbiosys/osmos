﻿using Osmos.Models.Identity;
using System.ComponentModel.DataAnnotations;

namespace Osmos.IdentityServer.Helpers
{
    public class CreateUserOptions {
        [Required]
        public IdentityUserManager IdentityUserManager { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string Rolename { get; set; }
        public bool? EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }
        public bool? PhoneNumberConfirmed { get; set; }
    }
}
