﻿using IdentityManager.Core.Logging;
using IdentityManager.Logging;
using Owin;
using Thinktecture.IdentityServer.AccessTokenValidation;
using Thinktecture.IdentityServer.Core.Configuration;
using Osmos.IdentityServer.Models;
using System;

namespace Osmos.IdentityServer
{
    public static class AppBuilderExtensions
    {
        public static void UseOsmosIdentityServer(this IAppBuilder app, OsmosIdentityServerOptions options)
        {
            if (options == null) throw new ArgumentNullException("OsmosIdentityServerOptions can not be null.");

            LogProvider.SetCurrentLogProvider(new DiagnosticsTraceLogProvider());

            var factory = Factory.Configure();
            factory.ConfigureUserService();
            app.Map("/identity", idsrvApp =>
            {
                idsrvApp.UseIdentityServer(new IdentityServerOptions
                {
                    SiteName = "Osmos IdentityServer",
                    IssuerUri = "https://Osmos.IdentityServer.net/",
                    SigningCertificate = options.SigningCertificate,
                    EnableWelcomePage = false,
                    Factory = factory,
                    RequireSsl = true
                });
            });

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = Discovery.Get().Authority,
                RequiredScopes = new[] { "resource_api" }
            });
        }
    }
}
