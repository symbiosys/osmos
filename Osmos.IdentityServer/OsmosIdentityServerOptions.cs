﻿using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;

namespace Osmos.IdentityServer
{
    public class OsmosIdentityServerOptions
    {
        public X509Certificate2 SigningCertificate { get; set; }
    }
}
