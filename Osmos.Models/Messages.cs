﻿
namespace Osmos.Models
{
    public static class Messages
    {
        public static string DataCanNotBeNull() {
            return "The data can not be null.";
        }

        public static string BadArguments() {
            return "Bad arguments.";
        }
    }
}
