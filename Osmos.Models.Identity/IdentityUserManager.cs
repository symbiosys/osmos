﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Configuration;

namespace Osmos.Models.Identity
{
    public class IdentityUserManager : UserManager<IdentityUser>
    {
        public IdentityUserManager(IdentityUserStore store)
            : base(store)
        {

        }

        public static IdentityUserManager Create(IdentityUserStore store)
        {

            string userLockoutEnabledByDefaultSetting = 
                ConfigurationManager.AppSettings["usermanager_UserLockoutEnabledByDefault"];
            string defaultAccountLockoutTimeSpanSetting =
                ConfigurationManager.AppSettings["usermanager_DefaultAccountLockoutTimeSpan"];
            string maxFailedAccessAttemptsBeforeLockoutSetting = 
                ConfigurationManager.AppSettings["usermanager_MaxFailedAccessAttemptsBeforeLockout"];
            string passwordRequiredLengthSetting = 
                ConfigurationManager.AppSettings["usermanager_PasswordRequiredLength"];
            string passwordRequireDigitSetting = 
                ConfigurationManager.AppSettings["usermanager_PasswordRequireDigit"];
            string passwordRequireLowercaseSetting = 
                ConfigurationManager.AppSettings["usermanager_PasswordRequireLowercase"];
            string passwordRequireNonLetterOrDigitSetting = 
                ConfigurationManager.AppSettings["usermanager_PasswordRequireNonLetterOrDigit"];
            string passwordRequireUppercaseSetting = 
                ConfigurationManager.AppSettings["usermanager_PasswordRequireUppercase"];

            IdentityUserManager mgr = new IdentityUserManager(store);

            mgr.UserLockoutEnabledByDefault = userLockoutEnabledByDefaultSetting == null ?
                                                true : Convert.ToBoolean(userLockoutEnabledByDefaultSetting);
            mgr.DefaultAccountLockoutTimeSpan = defaultAccountLockoutTimeSpanSetting == null ?
                                                TimeSpan.FromMinutes(15) : TimeSpan.Parse(defaultAccountLockoutTimeSpanSetting);
            mgr.MaxFailedAccessAttemptsBeforeLockout = maxFailedAccessAttemptsBeforeLockoutSetting == null ?
                                                3 : Convert.ToInt32(maxFailedAccessAttemptsBeforeLockoutSetting);
            mgr.PasswordValidator = new PasswordValidator {
                RequiredLength = passwordRequiredLengthSetting == null ?
                                    6 : Convert.ToInt32(passwordRequiredLengthSetting),
                RequireDigit = passwordRequireDigitSetting == null ?
                                    true : Convert.ToBoolean(passwordRequireDigitSetting),
                RequireLowercase = passwordRequireLowercaseSetting == null ?
                                    true : Convert.ToBoolean(passwordRequireLowercaseSetting),
                RequireNonLetterOrDigit = passwordRequireNonLetterOrDigitSetting == null ?
                                    true : Convert.ToBoolean(passwordRequireNonLetterOrDigitSetting),
                RequireUppercase = passwordRequireUppercaseSetting == null ?
                                    true : Convert.ToBoolean(passwordRequireUppercaseSetting)
            };

            return mgr;
        }
    }
}
