﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Osmos.Models.Identity
{
    public class IdentityUserStore : UserStore<IdentityUser>
    {
        public IdentityUserStore(IdentityDbContext ctx)
            : base(ctx)
        {

        }
    }
}
