﻿using System;
using Microsoft.AspNet.Identity;

namespace Osmos.Models.Identity
{
    public class IdentityUserManagerOptions
    {
        public bool UserLockoutEnabledByDefault { get; set; }
        public TimeSpan DefaultAccountLockoutTimeSpan { get; set; }
        public int MaxFailedAccessAttemptsBeforeLockout { get; set; }
        public PasswordValidator PasswordValidator { get; set; }
    }
}
