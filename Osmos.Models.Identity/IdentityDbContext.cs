﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Osmos.Models.Identity
{
    public class IdentityDbContext : Microsoft.AspNet.Identity.EntityFramework.IdentityDbContext<IdentityUser>
    {
        public DbSet<IdentityUserClaim> AspNetUserClaims { get; set; }

        public IdentityDbContext()
            : base("IdentityConnection", throwIfV1Schema: false)
        {

        }
    }
}
