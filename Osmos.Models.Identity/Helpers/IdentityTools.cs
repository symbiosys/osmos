﻿using Microsoft.AspNet.Identity.EntityFramework;
using Osmos.Models.Identity.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Osmos.Models.Identity.Helpers
{
    public static class IdentityTools
    {
        private static IdentityUserViewModel UserFromDbToViewModel(IdentityUser userFromDb) {
            return new IdentityUserViewModel {
                UserName = userFromDb.UserName,
                Email = userFromDb.Email,
                EmailConfirmed = userFromDb.EmailConfirmed
            };
        }

        public static async Task<IdentityUserViewModel> GetUserFromDbAsync(IdentityDbContext identityContext, string userId)
        {
            var user = await identityContext
                            .Users
                            .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null) throw new ArgumentNullException();

            return UserFromDbToViewModel(user);
        }
    }
}
